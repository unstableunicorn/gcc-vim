#!/bin/bash
# generate some keys when launched
ssh-keygen -q -t rsa -N '' -f ~/.ssh/id_rsa

if [ $# -gt 0 ]; then
    exec "$@"
fi

